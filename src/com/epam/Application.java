package com.epam;

import com.epam.util.HelloTalker;

public final class Application {
    public static void main(String[] args) {
        HelloTalker helloTalker = new HelloTalker("Hi", "dude", "!");
        helloTalker.sayHello();
    }
}
