package com.epam.util;

public final class HelloTalker {
    private String helloString;
    private String helloWord = "hello";
    private String targetWord = "world";
    private String endSign = "!";

    public HelloTalker(String helloWord, String targetWord, String endSign) {
        this.helloWord = helloWord;
        this.targetWord = targetWord;
        this.endSign = endSign;
        updateHelloString();
    }
    public HelloTalker() {
        updateHelloString();
    }
    public void sayHello() {
        System.out.println(helloString);
    }
    private void updateHelloString() {
        this.helloString = String.format("%s, %s%s", helloWord, targetWord, endSign);
    }

    public String getHelloString() {
        return this.helloString;
    }
    public String getHelloWord() {
        return helloWord;
    }
    public void setHelloWord(String helloWord) {
        this.helloWord = helloWord;
        updateHelloString();
    }
    public String getTargetWord() {
        return targetWord;
    }
    public void setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        updateHelloString();
    }
    public String getEndSign() {
        return endSign;
    }

    public void setEndSign(String endSign) {
        this.endSign = endSign;
        updateHelloString();
    }

}
